﻿using Workstation.ServiceModel.Ua;

namespace Opc
{
    /// <summary>
    /// Represents a client used to R/W in a OPC communication
    /// </summary>
    public class OpcAdapter
    {
        #region PROPERTIES

        /// <summary>
        /// Communication channel used to communicate with the OPC server
        /// </summary>
        private Workstation.ServiceModel.Ua.Channels.UaTcpSessionChannel channel;

        /// <summary>
        /// Indicates the symbolic name of the application that use this client
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// Indicates the IP address of the OPC server
        /// </summary>
        public string ServerIP { get; set; }

        /// <summary>
        /// Indicates the port number of the OPC server
        /// </summary>
        public int ServerPort { get; set; }

        #endregion

        /// <summary>
        /// Create a new OPC Adapter
        /// </summary>
        /// <param name="client_name">Indicates the symbolic name of the application that use this client</param>
        /// <param name="server_ip">Indicates the IP address of the OPC server</param>
        /// <param name="server_port">Indicates the port number of the OPC server</param>
        public OpcAdapter(string client_name, string server_ip, int server_port)
        {
            this.ClientName = client_name;
            this.ServerIP = server_ip;
            this.ServerPort = server_port;

            //  Define the Application Description object used to identifier the client in the OPC communication
            Workstation.ServiceModel.Ua.ApplicationDescription cliDescr = new Workstation.ServiceModel.Ua.ApplicationDescription
            {
                ApplicationName = this.ClientName,
                ApplicationUri = string.Format("urn:{0}:{1}", System.Net.Dns.GetHostName(), this.ClientName),
                ApplicationType = Workstation.ServiceModel.Ua.ApplicationType.Client
            };

            //  Define the OPC channel used in the communication
            channel = new Workstation.ServiceModel.Ua.Channels.UaTcpSessionChannel
            (
                cliDescr,
                null,
                new Workstation.ServiceModel.Ua.AnonymousIdentity(),
                string.Format("opc.tcp://{0}:{1}", this.ServerIP, this.ServerPort),
                Workstation.ServiceModel.Ua.SecurityPolicyUris.None
            );
        }

        #region CONNECTION FUNCTIONS

        /// <summary>
        /// Open the communication channel with the OPC server
        /// </summary>
        public void Open()
        {
            //  Start to open the OPC connection and wait until the opening process ends  
            this.channel.OpenAsync().Wait();
        }

        /// <summary>
        /// Close the communication channel with the OPC server
        /// </summary>
        public void Close()
        {
            try
            {
                //  Start to close the OPC connection and wait until the opening process ends
                this.channel.CloseAsync().Wait();
            }
            catch (System.Exception)
            {
                //  Start to abort the OPC connection and wait until the opening process ends
                this.channel.AbortAsync().Wait();
            }
        }

        #endregion

        #region MONITOR FUNCTIONS

        /// <summary>
        /// Send to OPC server a request to monitor specific PLC tags (in Siemens PLC this function is useless)
        /// </summary>
        /// <param name="tags">Indicates which PLC tags the server has to monitor</param>
        public void MonitorItems(params string[] tags)
        {
            try
            {
                //  Create and send a subscription to the OPC server
                Workstation.ServiceModel.Ua.CreateSubscriptionRequest subscription = new Workstation.ServiceModel.Ua.CreateSubscriptionRequest
                {
                    RequestedPublishingInterval = 1000,
                    RequestedMaxKeepAliveCount = 30,
                    RequestedLifetimeCount = 30 * 3,
                    PublishingEnabled = true
                };
                Workstation.ServiceModel.Ua.CreateSubscriptionResponse sub = this.channel.CreateSubscriptionAsync(subscription).Result;

                #region DEFINE REQUEST ITEMS

                System.Collections.Generic.List<Workstation.ServiceModel.Ua.MonitoredItemCreateRequest> items =
                    new System.Collections.Generic.List<Workstation.ServiceModel.Ua.MonitoredItemCreateRequest>();

                foreach (string tag in tags)

                    items.Add(new Workstation.ServiceModel.Ua.MonitoredItemCreateRequest()
                    {
                        ItemToMonitor = new Workstation.ServiceModel.Ua.ReadValueId
                        {
                            AttributeId = Workstation.ServiceModel.Ua.AttributeIds.Value,
                            NodeId = Workstation.ServiceModel.Ua.NodeId.Parse(tag)
                        },
                        RequestedParameters = new Workstation.ServiceModel.Ua.MonitoringParameters
                        {
                            ClientHandle = 1,
                            QueueSize = 1,
                            DiscardOldest = true,
                            SamplingInterval = 1000
                        }
                    });

                #endregion

                //  Create and send the monitoring request to the OPC server
                Workstation.ServiceModel.Ua.CreateMonitoredItemsRequest req = new Workstation.ServiceModel.Ua.CreateMonitoredItemsRequest
                {
                    SubscriptionId = sub.SubscriptionId,
                    TimestampsToReturn = Workstation.ServiceModel.Ua.TimestampsToReturn.Both,
                    ItemsToCreate = items.ToArray()
                };
                this.channel.CreateMonitoredItemsAsync(req);
            }
            catch (System.Exception ex)
            {
                if (this.channel.State != Workstation.ServiceModel.Ua.CommunicationState.Opened)
                {
                    this.Open();
                    this.MonitorItems(tags);
                }
                else
                {
                    this.Close();
                    throw ex;
                }
            }
        }

        #endregion

        #region READ & WRITE FUNCTIONS

        /// <summary>
        /// Read a PLC tag from the OPC server
        /// </summary>
        /// <param name="tag">Indicates the symbolic name of the PLC tag to read</param>
        public T ReadTag<T>(string tag)
        {
            try
            {
                //  Create and send the read request to the OPC server
                Workstation.ServiceModel.Ua.ReadRequest request = new Workstation.ServiceModel.Ua.ReadRequest
                {
                    NodesToRead = new Workstation.ServiceModel.Ua.ReadValueId[]
                    {
                        new Workstation.ServiceModel.Ua.ReadValueId
                        {
                            NodeId = Workstation.ServiceModel.Ua.NodeId.Parse(tag),
                            AttributeId = Workstation.ServiceModel.Ua.AttributeIds.Value
                        }
                    }
                };
                Workstation.ServiceModel.Ua.ReadResponse response = this.channel.ReadAsync(request).Result;

                if (response.Results[0].StatusCode != 0)
                    throw new Opc.OpcException(response.Results[0].StatusCode);

                return this.Decode<T>(response.Results[0].Value);
            }
            catch (System.Exception ex)
            {
                if (this.channel.State != Workstation.ServiceModel.Ua.CommunicationState.Opened)
                {
                    this.Open();
                    return this.ReadTag<T>(tag);
                }
                else
                {
                    this.Close();
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Read one or more PLC tags from the OPC server
        /// </summary>
        /// <param name="tags">Indicates symbolic names of the PLC tags to read</param>
        public System.Collections.Generic.Dictionary<string, object> ReadTags(params string[] tags)
        {
            try
            {
                #region DEFINE REQUEST IDS

                Workstation.ServiceModel.Ua.ReadValueId[] ids = new Workstation.ServiceModel.Ua.ReadValueId[tags.Length];

                for (int i = 0; i < tags.Length; i++)

                    ids[i] = new Workstation.ServiceModel.Ua.ReadValueId
                    {
                        NodeId = Workstation.ServiceModel.Ua.NodeId.Parse(tags[i]),
                        AttributeId = Workstation.ServiceModel.Ua.AttributeIds.Value
                    };

                #endregion

                //  Create and send the read request to the OPC server
                Workstation.ServiceModel.Ua.ReadRequest request = new Workstation.ServiceModel.Ua.ReadRequest { NodesToRead = ids };
                Workstation.ServiceModel.Ua.ReadResponse response = this.channel.ReadAsync(request).Result;

                #region CREATE THE RESULT DICTIONARY

                System.Collections.Generic.Dictionary<string, object> result = new System.Collections.Generic.Dictionary<string, object>();

                for (int i = 0; i < response.Results.Length; i++)
                {
                    if (response.Results[i].StatusCode != 0)
                        throw new Opc.OpcException(response.Results[i].StatusCode);

                    result.Add(tags[i], response.Results[i].Value);
                }

                #endregion

                return result;
            }
            catch (System.Exception ex)
            {
                if (this.channel.State != Workstation.ServiceModel.Ua.CommunicationState.Opened)
                {
                    this.Open();
                    return this.ReadTags(tags);
                }
                else
                {
                    this.Close();
                    throw ex;
                }
            }
        }


        /// <summary>
        /// Write a PLC tag in the OPC server
        /// </summary>
        /// <param name="tag">Indicates the symbolic name of the PLC tag to read</param>
        /// <param name="value">Indicates the value to write in the PLC tag</param>
        public void WriteTag<T>(string tag, T value)
        {
            try
            {
                //  Create and send the write request to the OPC server
                Workstation.ServiceModel.Ua.WriteRequest request = new Workstation.ServiceModel.Ua.WriteRequest
                {
                    NodesToWrite = new Workstation.ServiceModel.Ua.WriteValue[]
                    {
                        new Workstation.ServiceModel.Ua.WriteValue
                        {
                            NodeId = Workstation.ServiceModel.Ua.NodeId.Parse(tag),
                            AttributeId = Workstation.ServiceModel.Ua.AttributeIds.Value,
                            Value = new Workstation.ServiceModel.Ua.DataValue(this.Encode<T>(value))
                        }
                    }
                };
                Workstation.ServiceModel.Ua.WriteResponse response = this.channel.WriteAsync(request).Result;

                if (response.Results[0].Value != 0)
                    throw new Opc.OpcException(response.Results[0].Value);
            }
            catch (System.Exception ex)
            {
                if (this.channel.State != Workstation.ServiceModel.Ua.CommunicationState.Opened)
                {
                    this.Open();
                    this.WriteTag<T>(tag, value);
                }
                else
                {
                    this.Close();
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Write one or more PLC tag in the OPC server
        /// </summary>
        /// <param name="tags">Indicates a dictionary of tags and their values to write</param>
        public void WriteTags(System.Collections.Generic.Dictionary<string, object> tags)
        {
            try
            {
                #region DEFINE REQUEST VALUES

                Workstation.ServiceModel.Ua.WriteValue[] values = new Workstation.ServiceModel.Ua.WriteValue[tags.Count];

                int c = 0;
                foreach (string tag in tags.Keys)
                {
                    values[c] = new Workstation.ServiceModel.Ua.WriteValue
                    {
                        NodeId = Workstation.ServiceModel.Ua.NodeId.Parse(tag),
                        AttributeId = Workstation.ServiceModel.Ua.AttributeIds.Value,
                        Value = new Workstation.ServiceModel.Ua.DataValue(tags[tag])
                    };
                    c++;
                }

                #endregion

                //  Create and send the write request to the OPC server
                Workstation.ServiceModel.Ua.WriteRequest request = new Workstation.ServiceModel.Ua.WriteRequest { NodesToWrite = values };
                Workstation.ServiceModel.Ua.WriteResponse response = this.channel.WriteAsync(request).Result;

                for (int i = 0; i < response.Results.Length; i++)
                {
                    if (response.Results[i].Value != 0)
                        throw new Opc.OpcException(response.Results[i].Value);
                }
            }
            catch (System.Exception ex)
            {
                if (this.channel.State != Workstation.ServiceModel.Ua.CommunicationState.Opened)
                {
                    this.Open();
                    this.WriteTags(tags);
                }
                else
                {
                    this.Close();
                    throw ex;
                }
            }
        }

        #endregion

        #region ENCODING FUNCTIONS

        /// <summary>
        /// Encode a variable in the appropriate format for a write operation in the OPC server
        /// </summary>
        /// <typeparam name="T">Indicates the type of the variable to encode</typeparam>
        /// <param name="request">Indicates the value of the variable to encode</param>
        public object Encode<T>(T request)
        {
            object result = null;

            //  The value to write is an array of Structure
            if (typeof(T).IsArray && typeof(T).GetElementType().BaseType == typeof(Workstation.ServiceModel.Ua.Structure))
            {
                Workstation.ServiceModel.Ua.ExtensionObject[] objs =
                    new Workstation.ServiceModel.Ua.ExtensionObject[(request as System.Array).GetLength(0)];

                for (int i = 0; i < objs.Length; i++)
                {
                    object item = (request as System.Array).GetValue(i);
                    objs[i] = new Workstation.ServiceModel.Ua.ExtensionObject(item as Workstation.ServiceModel.Ua.Structure);
                }

                result = objs;
            }

            //  The value to write is a Structure
            else if (typeof(T).BaseType == typeof(Workstation.ServiceModel.Ua.Structure))
            {
                Workstation.ServiceModel.Ua.ExtensionObject obj =
                    new Workstation.ServiceModel.Ua.ExtensionObject(request as Workstation.ServiceModel.Ua.Structure);
                result = obj;
            }

            //  The value to write is a DataValue or an array of DataValue
            else
                result = request;

            return result;
        }

        /// <summary>
        /// Decode a variable to the appropriate format for a read operation in the OPC server
        /// </summary>
        /// <typeparam name="T">Indicates the type of the variable to decode</typeparam>
        /// <param name="response">Indicates the value of the variable to decode</param>
        public T Decode<T>(object response)
        {
            T result = default(T);

            //  The value to read is an array of Structure
            if (typeof(T).IsArray && typeof(T).GetElementType().BaseType == typeof(Workstation.ServiceModel.Ua.Structure))
            {
                Workstation.ServiceModel.Ua.ExtensionObject[] objs = (Workstation.ServiceModel.Ua.ExtensionObject[])response;
                object arr = System.Activator.CreateInstance(typeof(T), new object[] { objs.Length });

                Workstation.ServiceModel.Ua.Channels.BinaryDecoder decoder;
                object item;

                for (int i = 0; i < objs.Length; i++)
                {
                    item = System.Activator.CreateInstance(typeof(T).GetElementType());
                    decoder = new Workstation.ServiceModel.Ua.Channels.BinaryDecoder(new System.IO.MemoryStream((byte[])objs[i].Body));
                    (item as Workstation.ServiceModel.Ua.Structure).Decode(decoder);

                    (arr as System.Array).SetValue(item, i);
                }

                result = (T)arr;
            }

            //  The value to read is a Structure
            else if (typeof(T).BaseType == typeof(Workstation.ServiceModel.Ua.Structure))
            {
                Workstation.ServiceModel.Ua.ExtensionObject obj = (Workstation.ServiceModel.Ua.ExtensionObject)response;
                Workstation.ServiceModel.Ua.Channels.BinaryDecoder decoder =
                    new Workstation.ServiceModel.Ua.Channels.BinaryDecoder(new System.IO.MemoryStream((byte[])obj.Body));

                object el = System.Activator.CreateInstance(typeof(T));
                (el as Workstation.ServiceModel.Ua.Structure).Decode(decoder);

                result = (T)el;
            }

            //  The value to read is a DataValue or an array of DataValue
            else
                result = (T)response;

            return result;
        }

        #endregion
    }
}
